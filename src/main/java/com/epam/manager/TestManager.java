package com.epam.manager;

import com.epam.pages.EmailPage;
import com.epam.pages.GmailLoginPage;

public class TestManager {

    private String numberOfEmails;

    public void openGmailAndLogin(String email, String password) {
        GmailLoginPage gmailLoginPage = new GmailLoginPage();
        gmailLoginPage.fillEmailInputField(email);
        gmailLoginPage.clickByEmailNextButton();
        gmailLoginPage.fillPasswordInputField(password);
        gmailLoginPage.clickByPasswordNextButton();
        numberOfEmails = gmailLoginPage.getNumberOfEmailsBeforeSending();
    }

    public void clickByComposeButton() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByComposeButton();
    }

    public void fillEmailFields() {
        EmailPage emailPage = new EmailPage();
        emailPage.fillRecipientEmailInputField();
        emailPage.fillSubjectNameInputField();
        emailPage.fillTextAreaInputField();
    }

    public void sendMessageFromDraftFolder() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByDraftMessage();
        emailPage.clickBySendButton();
    }

    public void closeNewMessageWindow() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByCloseButton();
    }

    public void verifyThatMessageSavedAsDraft() {
       EmailPage emailPage = new EmailPage();
       emailPage.verifyIsMessageInDraftFolder(numberOfEmails);
   }
}
