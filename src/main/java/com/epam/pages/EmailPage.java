package com.epam.pages;

import com.github.javafaker.Faker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

//import static com.epam.utils.DriverManager.getDriver;
import java.util.List;

import static com.epam.utils.DriverManager.getWebDriver;

public class EmailPage {

    private static final Logger logger = LogManager.getLogger(EmailPage.class);
    private WebDriverWait wait = new WebDriverWait(getWebDriver(), 100);
    private Faker fake = new Faker();

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
    private WebElement composeButton;

    @FindBy(xpath = "//div[@class='wO nr l1']//textarea")
    private WebElement recipientEmailInputField;

    @FindBy(xpath = "//div[@class='aoD az6']//input")
    private WebElement subjectNameInputField;

    @FindBy(xpath = "//div[@class='Am Al editable LW-avf tS-tW']")
    private WebElement textAreaInputField;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
    private WebElement sendButton;

    @FindBy(xpath = "//span//a[contains(text(),'Надіслані')]")
    private WebElement sentEmailsButton;

    @FindBy(name = "volodya2127")
    private WebElement spanRecepient;

    @FindBy(className = "Ha")
    private WebElement closeButton;

    public EmailPage() {
        PageFactory.initElements(getWebDriver(), this);
    }

    public void clickByComposeButton() {
        logger.info("Click by compose button");
        wait.until(ExpectedConditions.visibilityOf(composeButton)).click();
    }

    public void clickBySendButton() {
        logger.info("Click by send button");
        sendButton.click();
    }

    public void fillRecipientEmailInputField() {
        logger.info("Fill recipient address");
        wait.until(ExpectedConditions.elementToBeClickable(recipientEmailInputField));
        recipientEmailInputField.sendKeys("volodya2127@gmail.com");
    }

    public void fillSubjectNameInputField() {
        logger.info("Fill subject field");
        subjectNameInputField.sendKeys(fake.internet().domainName());
    }

    public void fillTextAreaInputField() {
        logger.info("Fill text area");
        textAreaInputField.click();
        textAreaInputField.sendKeys(fake.internet().macAddress());
    }

    public void clickByCloseButton() {
        logger.info("Click by close button");
        closeButton.click();
    }

    public void verifyIsMessageInDraftFolder(String numberOfEmails) {
        int numberOne;
        int numberTwo = Integer.valueOf(numberOfEmails);

        logger.info("Verify is message in draft folder");
        numberOne = Integer.valueOf(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//span[@class='nU n1']//a[contains(text(),'Чернетки')]/ancestor::div[@class='aio UKr6le']//div"))).getText());


        System.out.println(numberOne + "=============");
        System.out.println(numberTwo + "=============");

        Assert.assertEquals(compareDigits(numberTwo,numberOne),1);
    }

    private int compareDigits(Integer numberOne, Integer numberTwo) {
        if(numberTwo>numberOne){
            return 1;
        }
        if(numberTwo<numberOne){
            return -1;
        }
        return 0;
    }

    public void clickByDraftMessage() {
        logger.info("Click by draft message");

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//span[@class='nU n1']//a[contains(text(),'Чернетки')]"))).click();

        Actions actions = new Actions(getWebDriver());
//        actions.moveToElement(getWebDriver().findElement(By.xpath(
//                "//div[contains(@class,'yW')]//span[contains(text(),'Чернетка')]"))).click().build().perform();

        actions.moveToElement(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//div[contains(@class,'yW')]//span[contains(text(),'Чернетка')]")))).click().build().perform();
    }
}
