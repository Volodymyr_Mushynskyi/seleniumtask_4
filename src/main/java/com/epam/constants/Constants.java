package com.epam.constants;

public class Constants {
    public static final String CHROME_NAME = "webdriver.chrome.driver";
    public static final String CHROME_DRIVER_LOCATION = "src/main/resources/chromedriver.exe";
    public static final int IMPLICITY_WAIT_VALUE = 10;
    public static final String BASE_URL = "https://mail.google.com/mail/";

    private Constants() {
    }
}
