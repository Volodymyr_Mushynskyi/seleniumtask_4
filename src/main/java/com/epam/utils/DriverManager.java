package com.epam.utils;

import com.epam.constants.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

    protected static void initDriver(final String browserName) {

        if ((Constants.CHROME_NAME).equalsIgnoreCase(browserName)) {
            System.setProperty(Constants.CHROME_NAME, Constants.CHROME_DRIVER_LOCATION);
            webDriver.set(new ChromeDriver());
        }
        webDriver.get().manage().window().maximize();
        webDriver.get().manage().timeouts().implicitlyWait(Constants.IMPLICITY_WAIT_VALUE, TimeUnit.SECONDS);
        webDriver.get().get(Constants.BASE_URL);
    }

    protected void quitDriver() {
        webDriver.get().quit();
    }

    public static WebDriver getWebDriver() {
        return webDriver.get();
    }
}
