package com.epam.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProjectProperties {

    private Properties prop = null;

    public ProjectProperties(){
        InputStream inputStream = null;
        try {
            this.prop = new Properties();
            inputStream = this.getClass().getResourceAsStream("/settings.properties");
            prop.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key){
        return this.prop.getProperty(key);
    }
}
