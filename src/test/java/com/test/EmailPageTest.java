package com.test;

import com.epam.manager.TestManager;
import com.epam.utils.BaseTest;
import com.epam.utils.ProjectProperties;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

public class EmailPageTest extends BaseTest {

    @DataProvider(parallel = true)
    public Iterator<Object[]> users() {
        ProjectProperties properties = new ProjectProperties();
        return Stream.of(
                new Object[] {properties.getProperty("first_email"),properties.getProperty("password")},
                new Object[] {properties.getProperty("second_email"),properties.getProperty("password")},
                new Object[] {properties.getProperty("third_email"),properties.getProperty("password")}).iterator();
    }

    @Test(dataProvider = "users")
    public void sendingEmail(String email, String password) {
        TestManager testManager = new TestManager();
        testManager.openGmailAndLogin(email,password);
        testManager.clickByComposeButton();
        testManager.fillEmailFields();
        testManager.closeNewMessageWindow();
        testManager.verifyThatMessageSavedAsDraft();
        testManager.sendMessageFromDraftFolder();
    }
}
